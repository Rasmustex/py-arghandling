from typing import Tuple, Any
from sys import stderr
from enum import Enum, auto
from dataclasses import dataclass

SUPPORTED_ARGS = [bool, str, int]
FLAG_HELPS = ""

@dataclass
class CmdOption:
    def __init__(self, sh: str, ln: str, typ: type, dflt: Any, hlp: str):
        if sh is not None:
            assert sh.startswith("-"), f"Expected short flag `{sh}` to start with `-`"
        assert ln is not None
        assert ln[:2] == "--", f"Expected long flag `{ln}` to start with `--`"
        self.sh = sh
        self.ln = ln
        self.typ = typ
        self.hlp = hlp
        self.dflt = dflt
        if typ in SUPPORTED_ARGS:
            assert type(dflt) is typ or dflt is None, f"Argument type of `{ln}` specified as `{typ}`, but default is {type(dflt)}"
        else:
            raise NotImplementedError(f"Argument type {typ} currently not handled")
        self.st = False
        self.value: Any = None

    def __str__(self):
        return f"CmdOption: (short: {self.sh}, long: {self.ln}, type: {self.typ}, default: {self.dflt}, set: {self.st}, value: {self.value})"

class CmdParser:
    def __init__(self, options: dict[str, CmdOption] = {}):
        self.options = options

    def add_arg(self, option: CmdOption):
        self.options[option.ln[2:]] = option

    def create_and_add_arg(self, sh: str, ln: str, typ: type, dflt: Any, hlp: str):
        self.options[ln[2:]] = CmdOption(sh, ln, typ, dflt, hlp)

    def get_option(self, option_name: str) -> CmdOption:
        return self.options[option_name]

    def get_option_value(self, option_name: str) -> Any:
        Option = self.get_option(option_name)
        if Option.st:
            return Option.value
        else:
            return Option.dflt

    def set_option_value(self, option_name: str, value: Any):
        Option = self.get_option(option_name)
        assert not Option.st, f"{option_name} has already been set once"
        if type(value) != Option.typ:
            raise TypeError(f"Type of desired value for option `{option_name}` expected to be `{Option.typ}`, but got `{type(value)}`")
        else:
            self.options[option_name].value = value
            self.options[option_name].st = True

    def __str__(self):
        string = "Argcollection: {"
        for key in self.options:
            Option = self.options[key]
            string += f"\n\t{key}: {Option}"
        string += "\n}"
        return string

    def parse_args(self, args: list[str]) -> Tuple[str, list[str]]:
        global FLAG_HELPS

        progname = args[0]
        args = args[1:]

        args, self = recurse_over_args(args, self)

        for option_name in self.options:
            Option = self.get_option(option_name)
            if Option.sh is not None:
                FLAG_HELPS += f"    {Option.sh : <2}, {Option.ln + ': ' : <20} type: {Option.typ.__name__ + ' ' : <5}- {Option.hlp}\n"
            else:
                FLAG_HELPS += f"    {Option.ln + ': ' : <20} type: {Option.typ.__name__ + ' ' : <5}- {Option.hlp}\n"

        return progname, args

# TODO: Cleanup
def recurse_over_args(args: list[str], Parser: CmdParser, index: int = 0) -> Tuple[list[str], CmdParser]:
    if index >= len(args):
        return (args, Parser)
    else:
        arg = args[index]
        if arg.startswith("-"):
            option_matched = False
            for option_name in Parser.options:
                Option = Parser.get_option(option_name)
                if Option.ln == arg or Option.sh == arg:
                    option_matched = True
                    if Option.typ == bool:
                        Parser.set_option_value(option_name, True)
                        del args[index]
                    elif Option.typ == str:
                        try:
                            Parser.set_option_value(option_name, args[index + 1])
                            del args[index:index + 2]
                        except IndexError:
                            raise IndexError(f"No string specified for string Option `{arg}`")
                    elif Option.typ == int:
                        try:
                            try:
                                value = int(args[index + 1])
                            except ValueError:
                                raise ValueError(f"Option `{arg}` expects `int` argument, but found `{args[index+1]}`")

                            Parser.set_option_value(option_name, value)
                            del args[index:index + 2]
                        except IndexError:
                            raise IndexError(f"No argument specified for int Option `{arg}`")
                    else:
                        raise TypeError(f"Option `{Option}`: option type `{Option.typ}` not yet implemented")
                    break
            if not option_matched:
                assert False, f"Unrecognised option: `{arg}`"
        else:
            index += 1
        return recurse_over_args(args, Parser, index=index)

USAGE_STRING = "[OPTIONS] ..."
def print_help(progname: str, usage_string_set: str = None):
    global FLAG_HELPS, USAGE_STRING

    if usage_string_set is not None:
        USAGE_STRING = usage_string_set

    print(f"Usage: {progname} {USAGE_STRING}", file=stderr)
    if FLAG_HELPS != "":
        print("Options:", file=stderr)
        print(FLAG_HELPS, file=stderr)
